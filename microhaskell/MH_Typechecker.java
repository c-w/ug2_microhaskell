import java.util.* ;
import java.io.* ;

class MH_Typechecker
{
    static MH_Parser MH_Parser = MH_Type_Impl.MH_Parser ;
    
    // The core of the typechecker:
    // Computing the MH_TYPE of a given MH_EXP in a given TYPE_ENV.
    // Should raise TypeError if MH_EXP isn't well-typed
    
    static MH_TYPE IntegerType = MH_Type_Impl.IntegerType ;
    static MH_TYPE BoolType    = MH_Type_Impl.BoolType ;
    
    static MH_TYPE computeType(MH_EXP exp, TYPE_ENV env) throws TypeError, UnknownVariable
    {
        if (exp.isVAR())
        {
            return env.typeOf(exp.value());
        }
        else if (exp.isNUM())
        {
            String value = exp.value();
            
            for (char c : value.toCharArray())
            {
                if (!(c >= '0' && c <= '9'))
                {
                    throw new TypeError("IntegerType " + value + " contains non-digit character " + c);
                }
            }
            
            return IntegerType;
        }
        else if (exp.isBOOLEAN())
        {
            String value = exp.value();
            
            if (!(value.equals("True") || value.equals("False")))
            {
                throw new TypeError("BoolType " + value + " does not match \"True\" or \"False\"");
            }
            
            return BoolType;
        }
        else if (exp.isAPP())
        {
            MH_TYPE firstType   = computeType(exp.first(), env);
            MH_TYPE secondType  = computeType(exp.second(), env);
            
            if (!firstType.isArrow())
            {
                throw new TypeError("Using " + secondType + " as function");
            }
            
            if (!firstType.left().equals(secondType))
            {
                throw new TypeError("Argument type does not match expected type");
            }
            
            return firstType.right();
        }
        else if (exp.isINFIX())
        {
            char infixOp = exp.infixOp();
            boolean firstIsIntegerType  = computeType(exp.first(),  env).equals(IntegerType);
            boolean secondIsIntegerType = computeType(exp.second(), env).equals(IntegerType);
            
            if (!(firstIsIntegerType && secondIsIntegerType))
            {
                throw new TypeError("Infix " + infixOp + " applied to non IntegerType arguments");
            }
            
            switch (infixOp)
            {
                case '=':
                case '<':    return BoolType;
                case '+':
                case '-':
                case '*':    return IntegerType;
                
                default:    throw new TypeError("Infix operator " + infixOp + " not in language");
            }
        }
        else if (exp.isIF())
        {
            MH_TYPE firstType  = computeType(exp.first(),  env);
            MH_TYPE secondType = computeType(exp.second(), env);
            MH_TYPE thirdType  = computeType(exp.third(),  env);
            
            if (!firstType.equals(BoolType))
            {
                throw new TypeError("if-then-else expression doesn't begin with boolean condition");
            }
            
            if (!secondType.equals(thirdType))
            {
                throw new TypeError("Types of \"then\" and \"else\" in if-then-else expression differ");
            }
            
            return thirdType;
        }
        else
        {
            throw new TypeError("Expression not in language");
        }
    }


    // Type environments:

    interface TYPE_ENV {
    MH_TYPE typeOf (String var) throws UnknownVariable ;
    }

    static class MH_Type_Env implements TYPE_ENV {

    TreeMap env ;

    public MH_TYPE typeOf (String var) throws UnknownVariable {
        MH_TYPE t = (MH_TYPE)(env.get(var)) ;
        if (t == null) throw new UnknownVariable(var) ;
        else return t ;
    }

    // Constructor for cloning a type env
    MH_Type_Env (MH_Type_Env given) {
        this.env = (TreeMap)given.env.clone() ;
    }

    // Constructor for building a type env from the type decls 
    // appearing in a program
    MH_Type_Env (TREE prog) throws DuplicatedVariable {
        this.env = new TreeMap() ;
        TREE prog1 = prog ;
        while (prog1.getRhs() != MH_Parser.epsilon) {
        TREE typeDecl = prog1.getChildren()[0].getChildren()[0] ;
        String var = typeDecl.getChildren()[0].getValue() ;
        MH_TYPE theType = MH_Type_Impl.convertType 
            (typeDecl.getChildren()[2]);
        if (env.containsKey(var)) 
            throw new DuplicatedVariable(var) ;
        else env.put(var,theType) ;
        prog1 = prog1.getChildren()[1] ;
        }
        System.out.println ("Type conversions successful.") ;
    }

    // Augmenting a type env with a list of function arguments.
    // Takes the type of the function, returns the result type.
    MH_TYPE addArgBindings (TREE args, MH_TYPE theType) 
        throws DuplicatedVariable, TypeError {
        TREE args1=args ;
        MH_TYPE theType1 = theType ;
        while (args1.getRhs() != MH_Parser.epsilon) {
        if (theType1.isArrow()) {
            String var = args1.getChildren()[0].getValue() ;
            if (env.containsKey(var)) {
            throw new DuplicatedVariable(var) ;
            } else {
            this.env.put(var, theType1.left()) ;
            theType1 = theType1.right() ;
            args1 = args1.getChildren()[1] ;
            }
        } else throw new TypeError ("Too many function arguments");
        } ;
        return theType1 ;
    }
    }

    static MH_Type_Env compileTypeEnv (TREE prog) 
    throws DuplicatedVariable{
    return new MH_Type_Env (prog) ;
    }

    // Building a closure (using lambda) from argument list and body
    static MH_EXP buildClosure (TREE args, MH_EXP exp) {
    if (args.getRhs() == MH_Parser.epsilon) 
        return exp ;
    else {
        MH_EXP exp1 = buildClosure (args.getChildren()[1], exp) ;
        String var = args.getChildren()[0].getValue() ;
        return new MH_Exp_Impl (var, exp1) ;
    }
    }

    // Name-closure pairs (result of processing a TermDecl).
    static class Named_MH_EXP {
    String name ; MH_EXP exp ;
    Named_MH_EXP (String name, MH_EXP exp) {
        this.name = name; this.exp = exp ;
    }
    }

    static Named_MH_EXP typecheckDecl (TREE decl, MH_Type_Env env) 
    throws TypeError, UnknownVariable, DuplicatedVariable,
           NameMismatchError {
    // typechecks the given decl against the env, 
    // and returns a name-closure pair for the entity declared.
    String theVar = decl.getChildren()[0].getChildren()[0].getValue();
    String theVar1= decl.getChildren()[1].getChildren()[0].getValue();
    if (!theVar.equals(theVar1)) 
        throw new NameMismatchError(theVar,theVar1) ; 
    MH_TYPE theType = 
        MH_Type_Impl.convertType (decl.getChildren()[0].getChildren()[2]) ;
    MH_EXP theExp =
        MH_Exp_Impl.convertExp (decl.getChildren()[1].getChildren()[3]) ;
    TREE theArgs = decl.getChildren()[1].getChildren()[1] ;
    MH_Type_Env theEnv = new MH_Type_Env (env) ;
    MH_TYPE resultType = theEnv.addArgBindings (theArgs, theType) ;
    MH_TYPE expType = computeType (theExp, theEnv) ;
    if (expType.equals(resultType)) {
        return new Named_MH_EXP (theVar,buildClosure(theArgs,theExp));
    }
    else throw new TypeError ("RHS of declaration of " +
                  theVar + " has wrong type") ;
    }

    static MH_Exp_Env typecheckProg (TREE prog, MH_Type_Env env)
    throws TypeError, UnknownVariable, DuplicatedVariable,
           NameMismatchError {
    TREE prog1 = prog ;
    TreeMap treeMap = new TreeMap() ;
    while (prog1.getRhs() != MH_Parser.epsilon) {
        TREE theDecl = prog1.getChildren()[0] ;
        Named_MH_EXP binding = typecheckDecl (theDecl, env) ;
        treeMap.put (binding.name, binding.exp) ;
        prog1 = prog1.getChildren()[1] ;
    }
    System.out.println ("Typecheck successful.") ;
    return new MH_Exp_Env (treeMap) ;
    }

    // For testing:

    public static void main (String[] args) throws Exception {
    Reader reader = new BufferedReader (new FileReader (args[0])) ;
    // try {
        LEX_TOKEN_STREAM MH_Lexer = 
        new CheckedSymbolLexer (new MH_Lexer (reader)) ;
        TREE prog = MH_Parser.parseTokenStream (MH_Lexer) ;
        MH_Type_Env typeEnv = compileTypeEnv (prog) ;
        MH_Exp_Env runEnv = typecheckProg (prog, typeEnv) ;
    // } catch (Exception x) {
        //  System.out.println ("MH Error: " + x.getMessage()) ;
    // }
    }
}
